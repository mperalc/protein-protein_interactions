.libPaths("/lustre/scratch117/cellgen/teamtrynka/marta/ppi_project/dependencies/R4_libs")

library(Seurat)
library(Rmagic)
library(SeuratDisk)
library(patchwork)
library(ggplot2)
gc()

args <- commandArgs(trailingOnly = TRUE)
data= args[1]
plot_dir = args[2]
output = args[3]

data = LoadH5Seurat(data)
Idents(data) = "cell_type"

DimPlot(data,reduction = "umap", label=TRUE) + NoLegend()
ggsave(paste0(plot_dir,"UMAP_preMagic.png"))
# Magic imputation
data.magic <- magic(data , genes="all_genes",
n.jobs=-2) # Using all available CPUs but one
DefaultAssay(data.magic) <- "MAGIC_RNA"  # Change default assay to imputed gene expression values for the magic object

# Memory B cell and Treg markers to compare imputed vs not imputed
Bmem_markers = c("CD27","CD19","CD40","IGHD","PAX5")
Treg_markers = c("CD25","CD4","FOXP3")

p1=FeatureScatter(data, feature1 = "CD27", feature2 = "CD19")
p2=FeatureScatter(data.magic, feature1 = "CD27", feature2 = "CD19")

png(paste0(plot_dir,"FeatureScatter_prePostMagic.png"),width = 10,height = 5,units = "in",res = 300)
p1+p2
dev.off()

saveRDS(data.magic, file = output)
