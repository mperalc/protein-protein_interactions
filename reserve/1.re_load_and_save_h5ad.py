# After conda activate /software/teamtrynka/conda/trynka-base

import sys
import anndata
import scanpy as sc

# Load covariate and phenotype
anndata = snakemake.input.anndata

out = snakemake.output.for_seurat



# Read anndata object
data = sc.read_h5ad(anndata)

# Sparse raw count matrix:
data.X
#per-cell metadata matrix:
data.obs
# Per-gene metadata matrix:
data.var

# To save files:
#sc.settings.autosave = True
#sc.settings.figdir = "../../data/scanpy_plots/"

#sc.pl.highest_expr_genes(data, n_top=20,save='.png', )

# See min number of cells with certain gene and min library size
#any(data.obs.n_counts < 1000)==True
#any(data.var.n_cells < 10)==True

#sc.pl.violin(data, ['n_counts', 'n_genes', 'percent_mito'],
#             jitter=0.4, multi_panel=True, save='.png')
# Normalize and log-transform - none of this have an effect on seurat conversion
#sc.pp.normalize_total(data, target_sum=1e4)
#sc.pp.log1p(data)
#data.raw = data

#umap-gives keyError, even though leiden is there in data.uns
#sc.pl.umap(data,color='leiden',save='.png')

data.write_h5ad(out)
