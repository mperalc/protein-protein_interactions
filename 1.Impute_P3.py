# impute in python
# Use the default python v. 3.7.4

import sys
sys.path.append('/lustre/scratch117/cellgen/teamtrynka/marta/ppi_project/dependencies/python3.7.4_libs')
import magic
import matplotlib.pyplot as plt
import pandas as pd
import scanpy as sc

anndata = snakemake.input.anndata

out = snakemake.output.only_python


# Read anndata object


data = sc.read_h5ad(anndata)

# Data should be pre-filtered and normalised already

# Create magic operator: default values: knn=5, knn_max = 3 * knn, decay=1, t=3.
magic_op = magic.MAGIC()
magic_op.set_params(n_jobs=-2)

data_magic = magic_op.fit_transform(data, genes='all_genes')

data.write_h5ad(out)

