#!/usr/bin/env python3
# Snakefile for protein-protein interaction scripts
# snakemake location:  /software/teamtrynka/conda/trynka-base/bin/snakemake

import os
import glob

# Get datsets from path. Make sure different parts of name are not separated by dots.
files=glob.glob("../../data/sc_data/original/*.h5ad")
ids =[os.path.basename(i) for i in files]
IDS=[i.split('.')[0] for i in ids]

rule all:
    input:
        file=expand("../../data/sc_data/{ids}_magic.h5ad", ids=IDS)


rule magic_impute_python:
    input:
        anndata="../../data/sc_data/original/{ids}.h5ad"
    output:
        only_python="../../data/sc_data/{ids}_magic.h5ad"
    message: "Impute gene expression with MAGIC in python. Uses default python. Run with snakemake --jobs 32 --cluster [comma] bsub {params.group} {params.queue} {params.cores} {params.memory} {params.jobname} {params.error} [comma]"
    threads: 32
    params:
        group= "-G teamtrynka",
        queue="-q normal",
        cores="-n 32",
        memory="-M400 -R'span[hosts=1] select[mem>400] rusage[mem=400]'",
        jobname= "-o ../../logs/log.magic_impute_python.%J.%I",
        error="-e ../../errors/error.magic_impute_python.%J.%I"
    script:"3.Impute_P3.py"
